# easy-chocolatey
Just some small batch scripts to make chocolatey supa-easier !
  * ci.bat
    - Shortcut script for command line "choco install (package)" => ci (package)
  * cil.bat
    - Custom script that parse a text file for packages to install them => cil (text file)
  * cis.bat
    - Shortcut script for command line "choco list -l" => cis
  * cu.bat
    - Shortcut script for command line "choco uninstall (package)" => cu (package)
  * cup.bat
    - Shortcut script for command line "choco upgrade (package)" => cup (package)
  * cupa.bat
    - Shortcut script for command line "choco upgrade all" => cupa
  * eis.bat
    - Custom script that list installed packages and export them to a text file => eis
  * eis2.bat
    - Just another version of eis.bat which perfoms more checks while analyzing => eis2

See https://github.com/chocolatey/choco for more details
